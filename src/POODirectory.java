import java.util.*;
public class POODirectory extends POODirItem{
	public static final int MAXITEM = 1024;
	private String directory_name;
	private LinkedList<POODirItem> itemList;
	private int itemNum;
	public POODirectory(String name) {
		property = 1;
		directory_name = name;
		itemList = new LinkedList<POODirItem>();
	}
	public void printout() {
		System.out.println(directory_name);
	}
	public void add(POOBoard board) {
		if(itemNum <= MAXITEM) {
			itemList.add(board);
			itemNum++;
		}
		else {
			System.out.println("Cannot add the board : Directory full");
		}
	}
	public void add(POODirectory dir) {
		if(itemNum <= MAXITEM) {
			itemList.add(dir);
			itemNum++;
		}
		else {
			System.out.println("Cannot add the directory : Directory full");
		}
	}
	public void add_split() {
		POODirLine line = new POODirLine();
		if(itemNum <= MAXITEM) {
			itemList.add(line);
			itemNum++;
		}
		else {
			System.out.println("Cannot add split line : Directory full");
		}
	}
	public void del(int pos) {
		if(pos >= 0 && pos < itemNum) {
			itemList.remove(pos);
			itemNum--;
		}
		else {
			System.out.println("Cannot delete : position out of range");
			System.out.println("< Press Enter to continue... >");
			Scanner scanner = new Scanner(System.in);
			String buf = scanner.nextLine();
		}
	}
	public void move(int src_pos, int des_pos) {
		POODirItem movItem;
		if(src_pos >= 0 && src_pos < itemNum && des_pos >= 0 && des_pos < itemNum) {
			if(src_pos != des_pos) {
				movItem = itemList.remove(src_pos);
				itemList.add(des_pos, movItem);
			}
			else {
				System.out.println("Cannot move : position range error.");
			}
		}
	}
	public int length() {
		return itemNum;
	}
	public void show() {
		int si = itemList.size();
		POODirItem[] showItem = itemList.toArray(new POODirItem[0]);
		POODirectory dtmp;
		POOBoard btmp;
		int i;
		System.out.printf("Directory %s content : \n", directory_name);
		System.out.println("=====================================================");
		for(i = 0; i < itemNum; i++) {
			System.out.printf("%d", i);
			if(showItem[i].property == 1) {
				System.out.printf("(D)");
				dtmp = (POODirectory) showItem[i];
				System.out.printf("\t");
				dtmp.printout();
			}
			else if(showItem[i].property == 2) {
				btmp = (POOBoard) showItem[i];
				System.out.printf("\t");
				btmp.printout();
			}
			else if(showItem[i].property == 3) {
				System.out.println(" ---------------------------------------------------");
			}
		}
		System.out.println("=====================================================");
	}
	public POODirItem get(int index) {
		return itemList.get(index);
	}
	public String get_name() {
		return directory_name;
	}
}
