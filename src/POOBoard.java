import java.util.*;
public class POOBoard extends POODirItem{
	public static final int MAXARTICLE = 1024;
	private boolean IDset[];
	private String board_name;
	private LinkedList<POOArticle> articleList;
	private int articleNum;
	public POOBoard(String name) {
		property = 2;
		int i;
		board_name = name;
		IDset = new boolean[MAXARTICLE];
		for(i = 0; i < MAXARTICLE; i++) {
			IDset[i] = false;
		}
		articleList = new LinkedList<POOArticle>();
	}
	public void printout() {
		System.out.println(board_name);
	}
	public void add(POOArticle article) {
		int i;
		boolean flag = false;
		for(i = 0; i < MAXARTICLE; i++) {
			if(IDset[i] == false) {
				flag = true;
				break;
			}
		}
		if(flag == true) {
			article.setID(i);
			IDset[i] = true;
			articleList.add(article);
			articleNum++;
		}
		else {
			System.out.println("Sorry, the number of articles in this board is full.");
		}
	}
	public void del(int pos) {
		if(pos >= articleNum || pos < 0) {
			System.out.println("Article cannot be deleted : position out of range.");
			System.out.println("< Press Enter to continue... >");
			Scanner scanner = new Scanner(System.in);
			String buf = scanner.nextLine();
		}
		else {
			POOArticle delArticle = articleList.remove(pos);
			IDset[delArticle.getID()] = false;
			articleNum--;
		}
	}
	public void move(int src_pos, int des_pos) {
		POOArticle movArticle;
		if(src_pos >= 0 && src_pos < articleNum && des_pos >= 0 && des_pos < articleNum) {
			if(src_pos != des_pos) {
				movArticle = articleList.remove(src_pos);
				articleList.add(des_pos, movArticle);
			}
		}
		else {
			System.out.println("Cannot move article : position range error.");
		}
	}
	public int length() {
		return articleNum;
	}
	public void show() {
		int si = articleList.size();
		POOArticle[] showArt = articleList.toArray(new POOArticle[0]);
		int i;
		System.out.printf("All articles in board %s\n", board_name);
		System.out.println("=====================================================");
		for(i = 0; i < articleNum; i++) {
			System.out.printf("%d\t%s\t", i, showArt[i].getAuthor());
			System.out.println(showArt[i].getTitle());
		}
		System.out.println("=====================================================");
	}
	public String get_name() {
		return board_name;
	}
	public POOArticle get(int pos) {
		return articleList.get(pos);
	}
	public void set(int pos, POOArticle a) {
		articleList.set(pos, a);
	}
}
