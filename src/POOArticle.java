import java.util.*;
public class POOArticle extends POODirItem{
	public static final int MAXEVAL = 80;
	private int ID;
	private String title;
	private String author;
	private LinkedList<String> content;
	private int EvaCount;
	private LinkedList<String> EvaMsgs;
	public POOArticle(String t, String a, LinkedList<String> c) {
		property = 4;
		title = t;
		author = a;
		content = c;
		EvaCount = 0;
		EvaMsgs = new LinkedList<String>();
	}
	public void setID(int idid) {
		ID = idid;
	}
	public int getID() {
		return ID;
	}
	public String getTitle() {
		return title;
	}
	public String getAuthor() {
		return author;
	}
	public void push(String msg) {
		int len = msg.length();
		if(len > MAXEVAL) {
			len = MAXEVAL;
		}
		int i;
		StringBuffer sb = new StringBuffer();
		for(i = 0; i < len; i++) {
			sb.append(msg.charAt(i));
		}
		EvaMsgs.add(sb.toString());
		EvaCount++;
	}
	public void boo(String msg) {
		int len = msg.length();
		if(len > MAXEVAL) {
			len = MAXEVAL;
		}
		int i;
		StringBuffer sb = new StringBuffer();
		for(i = 0; i < len; i++) {
			sb.append(msg.charAt(i));
		}
		EvaMsgs.add(sb.toString());
		EvaCount--;
	}
	public void arrow(String msg) {
		int len = msg.length();
		if(len > MAXEVAL) {
			len = MAXEVAL;
		}
		int i;
		StringBuffer sb = new StringBuffer();
		for(i = 0; i < len; i++) {
			sb.append(msg.charAt(i));
		}
		EvaMsgs.add(sb.toString());
	}
	public void show() {
		int EvaMsgLen = EvaMsgs.size();
		int ContentLines = content.size();
		System.out.printf(" \033[1;34;47mAuthor:\033[m\033[1;37;44m%s\033[m\n", author);
		System.out.printf(" \033[1;34;47mTitle: \033[m\033[1;37;44m%s\033[m\n", title);
		System.out.println("\033[1;36m-----------------------------------------------------\033[m");
		int i;
		int csi = content.size();
		int esi = EvaMsgs.size();
		String[] contentAry = content.toArray(new String[0]);
		String[] EvaMsgsAry = EvaMsgs.toArray(new String[0]);
		for(i = 0; i < ContentLines; i++) {
			System.out.println(contentAry[i]);
		}
		System.out.println("--");
		for(i = 0; i < EvaMsgLen; i++) {
			System.out.println(EvaMsgsAry[i]);
		}
	}
	public void list() {
		System.out.println("=====================================================");
		System.out.printf("Evaluation Count : %d\n", EvaCount);
		System.out.printf("ID : %d\n", ID);
		System.out.printf("Title: %s\n", title);
		System.out.printf("Author: %s\n", author);
		System.out.println("=====================================================");
	}
}
