import java.util.*;
public class POOBBS {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("#######################################################");
		System.out.println("#     Hello!! You are now in your home directory.     #");
		System.out.println("#             Please input your command.              #");
		System.out.println("#                                                     #");
		System.out.println("#     \033[1;32m1: Create a new board.\033[m                          #");
		System.out.println("#     \033[1;32m2: Create a new directory.\033[m                      #");
		System.out.println("#     Or \033[1;31mInput Other number to Exit.\033[m                  #");
		System.out.println("#######################################################");
		System.out.printf("Your command : ");
		int input_command = scanner.nextInt();
		String buf = scanner.nextLine();
		int where_am_I = 1;
		int pos, src_pos, des_pos;
		int ntmp;
		POODirectory root = new POODirectory("home_directory");
		POOBoard newb;
		POODirectory newd;
		POOArticle newa;
		POODirLine line;
		String bname;
		String dname;
		String atitle;
		String aauthor;
		String stmp;
		String stmp2;
		LinkedList<String> acontent; 
		POODirItem now = root;
		POODirItem tmp;
		POOArticle atmp;
		POOBoard btmp = new POOBoard("tmp");
		POODirectory dtmp = root;
		LinkedList<POODirItem> list = new LinkedList<POODirItem>();
		list.add(root);
		
		boolean byebye_flag = false;
		if(input_command == 1) {
			System.out.printf("\033[1;33m# Please input the name of the board you'd like to create : \033[m");
			bname = scanner.nextLine();
			newb = new POOBoard(bname);
			root.add(newb);
		}
		else if(input_command == 2) {
			System.out.printf("\033[1;33m# Please input the name of the directory you'd like to create : \033[m");
			dname = scanner.nextLine();
			newd = new POODirectory(dname);
			root.add(newd);
		}
		else {
			byebye_flag = true;
		}
		String nowin = "home_directory";
		while(byebye_flag == false) {
			switch (where_am_I) {
				case 1:
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("#############################################################");
					System.out.printf("# \033[1;35mYou are now in directory %s.\n\033[m", nowin);
					System.out.println("#############################################################");
					System.out.println("# Please input your command.                                #");
					System.out.println("# 1: Create a new board.                                    #");
					System.out.println("# 2: Create a new directory.                                #");
					System.out.println("# 3: Add a split line.                                      #");
					System.out.println("# 4: Delete a board/directory/split line.                   #");
					System.out.println("# 5: Move a board/directory/split line to another position. #");
					System.out.println("# 6: Show the length of this directory now.                 #");
					System.out.println("# 7: Enter a board/directory.                               #");
					System.out.println("# 8. Back.                                                  #");
					System.out.println("# Or \033[1;31mInput Other number to Exit.\033[m                            #");
					System.out.println("#############################################################");
					System.out.println("");
					System.out.println("");
					dtmp.show();
					System.out.println("");
					System.out.printf("# Your command : ");
					input_command = scanner.nextInt();
					buf = scanner.nextLine();
					switch (input_command) {
						case 1:
							where_am_I = 1;
							System.out.printf("\033[1;32m# Please input the name of the board you'd like to create : \033[m");
							bname = scanner.nextLine();
							newb = new POOBoard(bname);
							dtmp.add(newb);
							break;
						case 2:
							where_am_I = 1;
							System.out.printf("\033[1;32m# Please input the name of the directory you'd like to create : \033[m");
							dname = scanner.nextLine();
							newd = new POODirectory(dname);
							dtmp.add(newd);
							break;
						case 3:
							where_am_I = 1;
							dtmp.add_split();
							break;
						case 4:
							where_am_I = 1;
							System.out.printf("\033[1;32m# Please input the position of the item you'd like to delete : \033[m");
							pos = scanner.nextInt();
							buf = scanner.nextLine();
							dtmp.del(pos);
							break;
						case 5:
							where_am_I = 1;
							System.out.printf("\033[1;32m# Please input the positon of the item you'd like to move : \033[m");
							src_pos = scanner.nextInt();
							buf = scanner.nextLine();
							if(src_pos < 0 || src_pos >= dtmp.length()) {
								System.out.println("Error!!! position out of range.");
								System.out.println("< Press Enter to continue... >");
								buf = scanner.nextLine();
								break;
							}
							System.out.printf("\033[1;33m# Please input the destination positon you'd like to move to : \033[m");
							des_pos = scanner.nextInt();
							buf = scanner.nextLine();
							if(des_pos < 0 || des_pos >= dtmp.length()) {
								System.out.println("Error!!! position out of range.");
								System.out.println("< Press Enter to continue... >");
								buf = scanner.nextLine();
								break;
							}
							dtmp.move(src_pos, des_pos);
							break;
						case 6:
							where_am_I = 1;
							System.out.printf("\033[1;33m# The length is : %d\n\033[m", dtmp.length());
							System.out.println("< Press Enter to continue... >");
							buf = scanner.nextLine();
							break;
						case 7:
							System.out.printf("\033[1;32m# Please enter the index of item you'd like to enter : \033[m");
							pos = scanner.nextInt();
							buf = scanner.nextLine();
							if(pos >= 0 && pos < dtmp.length()) {
								tmp = dtmp.get(pos);
								list.add(tmp);
								if(tmp.property == 1) {
									dtmp = (POODirectory) tmp;
									nowin = dtmp.get_name();
									where_am_I = 1;
								}
								else if(tmp.property == 2) {
									btmp = (POOBoard) tmp;
									nowin = btmp.get_name();
									where_am_I = 2;
								}
								else {
									System.out.println("# Error!!! This is not a board/directory.");
									System.out.println("< Press Enter to continue... >");
									buf = scanner.nextLine();
								}
							}
							else {
								System.out.println("# Error!!! The position is out of range.");
								System.out.println("< Press Enter to continue... >");
								buf = scanner.nextLine();
							}
							break;
						case 8:
							if(list.size() > 1) {
								tmp = list.removeLast();
								now = list.peekLast();
								dtmp = (POODirectory) now;
								nowin = dtmp.get_name();
								where_am_I = 1;
							}
							break;
						default:
							byebye_flag = true;
							break;
					}
					break;
				case 2:
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("#############################################################");
					System.out.printf("# \033[1;35mYou are now in board %s.\n\033[m", nowin);
					System.out.println("#############################################################");
					System.out.println("# Please input your command.                                #");
					System.out.println("# 1: Add a new article.                                     #");
					System.out.println("# 2: Delete an article.                                     #");
					System.out.println("# 3: Move an article to another position.                   #");
					System.out.println("# 4: Show the length of this board now.                     #");
					System.out.println("# 5: Show an article content and evaluations in this board. #");
					System.out.println("# 6: Show an article information in this board.             #");
					System.out.println("# 7: Add an evaluation under an article.                    #");
					System.out.println("# 8. Back.                                                  #");
					System.out.println("# Or \033[1;31mInput Other number to Exit.\033[m                            #");
					System.out.println("#############################################################");
					System.out.println("");
					System.out.println("");
					btmp.show();
					System.out.println("");
					System.out.printf("# Your command : ");
					input_command = scanner.nextInt();
					buf = scanner.nextLine();
					switch (input_command) {
						case 1:
							where_am_I = 2;
							System.out.printf("\033[1;32m# Please input the author of the article you'd like to add : \033[m");
							aauthor = scanner.nextLine();
							System.out.printf("\033[1;33m# Please input the title of the article you'd like to add : \033[m");
							atitle = scanner.nextLine();
							System.out.println("\033[1;36m# Please input the content of the article.\033[m");
							System.out.println("# (Enter \033[1;31m\"EndquitQuit\"\033[m in a line to end the article.)");
							acontent = new LinkedList<String>();
							stmp = scanner.nextLine();
							while(stmp.compareTo("EndquitQuit") != 0) {
								acontent.add(stmp);
								stmp = scanner.nextLine();
							}
							newa = new POOArticle(atitle, aauthor, acontent);
							btmp.add(newa);
							break;
						case 2:
							where_am_I = 2;
							System.out.printf("\033[1;32m# Please input the position of the article you'd like to delete : \033[m");
							pos = scanner.nextInt();
							buf = scanner.nextLine();
							btmp.del(pos);
							break;
						case 3:
							where_am_I = 2;
							System.out.printf("\033[1;32m# Please input the positon of the article you'd like to move : \033[m");
							src_pos = scanner.nextInt();
							buf = scanner.nextLine();
							if(src_pos < 0 || src_pos >= btmp.length()) {
								System.out.println("Error!!! position out of range.");
								System.out.println("< Press Enter to continue... >");
								buf = scanner.nextLine();
								break;
							}
							System.out.printf("\033[1;32m# Please input the destination positon you'd like to move to : \033[m");
							des_pos = scanner.nextInt();
							buf = scanner.nextLine();
							if(des_pos < 0 || des_pos >= btmp.length()) {
								System.out.println("Error!!! position out of range.");
								System.out.println("< Press Enter to continue... >");
								buf = scanner.nextLine();
								break;
							}
							btmp.move(src_pos, des_pos);
							break;
						case 4:
							where_am_I = 2;
							System.out.printf("\033[1;33m# The length is : %d\n\033[m", btmp.length());
							System.out.println("< Press Enter to continue... >");
							buf = scanner.nextLine();
							break;
						case 5:
							System.out.printf("\033[1;32m# Please enter the index of article you'd like to read : \033[m");
							pos = scanner.nextInt();
							buf = scanner.nextLine();
							System.out.println("");
							System.out.println("");
							System.out.println("");
							if(pos >= 0 && pos < btmp.length()) {
								atmp = btmp.get(pos);
								atmp.show();
								where_am_I = 2;
							}
							else {
								System.out.println("# Error!!! The position is out of range.");
								System.out.println("< Press Enter to continue... >");
								buf = scanner.nextLine();
							}
							System.out.println("");
							System.out.println("");
							System.out.println("\033[m< Press Enter to continue... >");
							buf = scanner.nextLine();
							break;
						case 6:
							System.out.printf("\033[1;32m# Please enter the index of article you'd like to show information : \033[m");
							pos = scanner.nextInt();
							buf = scanner.nextLine();
							if(pos >= 0 && pos < btmp.length()) {
								atmp = btmp.get(pos);
								atmp.list();
								where_am_I = 2;
							}
							else {
								System.out.println("# Error!!! The position is out of range.");
								System.out.println("< Press Enter to continue... >");
								buf = scanner.nextLine();
							}
							System.out.println("< Press Enter to continue... >");
							buf = scanner.nextLine();
							break;
						case 7:
							System.out.printf("\033[1;32m# Please enter the index of article you'd like to add an evaluation : \033[m");
							pos = scanner.nextInt();
							buf = scanner.nextLine();
							if(pos >= 0 && pos < btmp.length()) {
								atmp = btmp.get(pos);
								where_am_I = 2;
								System.out.printf("\033[1;37m# Your username : \033[m");
								stmp2 = scanner.nextLine();
								stmp2 = "\033[1;33m"+stmp2;
								System.out.printf("\033[1;37m# You want to add a \033[1;33m(1) push\033[m \033[1;31m(2) boo\033[m \033[1;37m(3) arrow\033[m : \033[m");
								ntmp = scanner.nextInt();
								buf = scanner.nextLine();
								System.out.println("\033[1;32m# Please enter your evaluation message.\033[m");
								stmp = scanner.nextLine();
								if(ntmp == 1) {
									stmp = "\033[1;37m(P) \033[m"+stmp2+": "+stmp+"\033[m";
									atmp.push(stmp);
								}
								else if(ntmp == 2) {
									stmp = "\033[1;31m(B) \033[m"+stmp2+": "+stmp+"\033[m";
									atmp.boo(stmp);
								}
								else {
									stmp = "\033[1;31m--> \033[m"+stmp2+": "+stmp+"\033[m";
									atmp.arrow(stmp);
								}
								btmp.set(pos, atmp);
							}
							else {
								System.out.println("# Error!!! The position is out of range.");
								System.out.println("< Press Enter to continue... >");
								buf = scanner.nextLine();
							}
							break;
						case 8:
							if(list.size() > 1) {
								tmp = list.removeLast();
								now = list.peekLast();
								dtmp = (POODirectory) now;
								nowin = dtmp.get_name();
								where_am_I = 1;
							}
							break;
						default:
							byebye_flag = true;
							break;
					}
					break;
				default:
					break;
			}
		}
		System.out.println("ByeBye!!");
	}
}
